import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import nl.knaw.dans.common.dbflib.DbfLibException;
import nl.knaw.dans.common.dbflib.Field;
import nl.knaw.dans.common.dbflib.IfNonExistent;
import nl.knaw.dans.common.dbflib.Record;
import nl.knaw.dans.common.dbflib.Table;
import nl.knaw.dans.common.dbflib.ValueTooLargeException;

class SimpleFileChooser extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;
    JButton openButton = new JButton("Open File");
    
    JTextArea dest_folder=new JTextArea(2,35);
    JTextArea date_picker=new JTextArea(2,10);
    JLabel dest_path=new JLabel("Destination");
    JLabel date=new JLabel("Date");
    public SimpleFileChooser() {
    	super("PF ECR Converter- PI Jain & Co.");
    	setLocation(300, 200); 
    	setSize(800, 400);
    	FlowLayout experimentLayout = new FlowLayout();
    	setLayout(experimentLayout);
    	
    	setDefaultCloseOperation(EXIT_ON_CLOSE);
    	dest_folder.setLocation(20, 20);
    	Container c = getContentPane();
    	c.setLayout(new FlowLayout());
    	openButton.addActionListener(this);
  
    	c.add(dest_path);
    	c.add(dest_folder);
    	c.add(date);
    	c.add(date_picker);	
    	
    	c.add(openButton);
    	setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
    	
 
  }
   

   public static final int trimRight( byte[] bytes, int pos )
   {
       if ( bytes == null )
       {
           return pos;
       }

       while ( ( pos >= 0 ) && ( bytes[pos] == ' ' ) )
       {
           pos--;
       }

       return pos;
   }


@Override
public void actionPerformed(ActionEvent e) {
	if(e.getActionCommand()=="Open File")
	{
		
	 getfilepath();	
		
	}
	
	
	// TODO Auto-generated method stub
	
    
	
     }


private void getfilepath() {
    final JLabel statusbar = 
            new JLabel("Your Selection");  

	 JFileChooser chooser = new JFileChooser();
	    chooser.setMultiSelectionEnabled(true);
	    int option = chooser.showOpenDialog(SimpleFileChooser.this);
	    if (option == JFileChooser.APPROVE_OPTION) {
	    	
  	
	      File[] sf = chooser.getSelectedFiles();
	      
	      String filelist = "nothing";
	      if (sf.length > 0) filelist = sf[0].getName();
	     
	      
	      
	      
	      for (int l = 0; l < sf.length; l++) {

	        filelist += ", " + sf[l].getName();
	      
	        String filepath=sf[l].getAbsolutePath(); 
	        	
	        
	        
	        create_file(filepath,sf[l],dest_folder.getText());
	        
	        
	        System.out.println("Hey");
	        
	      }


	      statusbar.setText("You chose " + filelist);
	      
	    }
	    else {
	      statusbar.setText("You canceled.");
	    }

	
}


private void create_file(String filepath,File sf,String destfolder) {
    final Table table = new Table(new File(filepath));
    
    try
    {
        table.open(IfNonExistent.ERROR);

        final List<Field> fields = table.getFields();

     
     
        final Iterator<Record> recordIterator = table.recordIterator();

        
        
        FileOutputStream fop = null;
		File file;
      	
		String ext=sf.getName().substring(0,sf.getName().indexOf('.'));
		ext=ext.toLowerCase();
		System.out.println(destfolder.concat(ext).concat(".txt"));
		
		String date=date_picker.getText();
		file = new File(destfolder.concat(ext).concat("_").concat(date).concat(".txt"));
		fop = new FileOutputStream(file);
		int count=0;
		
		 for(@SuppressWarnings("unused") final Field field: fields)
       {
			 count++;
			 
			 
       }
		
		
		
		if (!file.exists()) {
			file.createNewFile();
		}

		
        while(recordIterator.hasNext())
        {
            final Record record = recordIterator.next();
           
            
            int i=0;
            for(final Field field: fields)
            {
                try
                {
              	  
              	  
          		byte[] one = record.getRawValue(field);
        			int pos=trimRight(one, one.length-1 );
        			byte[] output = new byte[pos+1];
        		    System.arraycopy(one, 0, output, 0, pos+1);
        		  
        		  i++;
        		  if(i!=count){
        		    
        		  byte[] combined = new byte["#~#".length() + output.length];
        		  System.arraycopy(output,0,combined,0 ,output.length);
        		  System.arraycopy("#~#".getBytes(),0,combined,output.length,"#~#".length());
        		  fop.write(combined);
        		  //System.out.println("ddd");
        			
        		  }
        		  else {
        			 fop.write(output);
        			  
        			  
        		  }
        		  
        		  
                	
                   
                
                }
                catch(ValueTooLargeException vtle)
                {
                    // Cannot happen :)
                }
            }

            fop.write("\r\n".getBytes());
            
           
        }
        fop.flush();
		fop.close();

//        System.out.println("--------------");
    }
    catch(IOException ioe)
    {
        System.out.println("Trouble reading table or table not found");
        ioe.printStackTrace();
    }
    catch(DbfLibException dbflibException)
    {
        System.out.println("Problem getting raw value");
        dbflibException.printStackTrace();
    }
    finally
    {
        try
        {
            table.close();
        } catch (IOException ex)
        {
            System.out.println("Unable to close the table");
        }
    }










	
}

 }
	

   
 
 