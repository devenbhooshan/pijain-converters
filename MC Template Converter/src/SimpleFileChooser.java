import java.awt.Container;

import java.awt.FlowLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;

import nl.knaw.dans.common.dbflib.DbfLibException;
import nl.knaw.dans.common.dbflib.Field;
import nl.knaw.dans.common.dbflib.IfNonExistent;
import nl.knaw.dans.common.dbflib.Record;
import nl.knaw.dans.common.dbflib.Table;
import nl.knaw.dans.common.dbflib.ValueTooLargeException;


class SimpleFileChooser extends JFrame implements ActionListener{
	
	
	private static final long serialVersionUID = 1L;
    JButton openButton = new JButton("Open");
    
    JTextArea dest_folder=new JTextArea(2,30);
    JTextArea date_picker=new JTextArea(2,10);
    JLabel dest_path=new JLabel("Destination");
    JTextArea source_folder=new JTextArea(2,20);
    JLabel date=new JLabel("Date");
    JLabel template_file=new JLabel("Template File");
    
    
    
    
   
     

    
  

    
    public SimpleFileChooser() {
    	
    	
    	
    	super("MC Template- PI Jain & Co.");
    /*	try {
			//Runtime.getRuntime().exec("cmd /c C:/Users/DEVEN/Desktop/deven.bat");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		*/
    	setLocation(300, 200); 
    	setSize(1000, 400);
    	setDefaultCloseOperation(EXIT_ON_CLOSE);
    	dest_folder.setLocation(20, 20);
    	Container c = getContentPane();
    	c.setLayout(new FlowLayout());
    	openButton.addActionListener(this);
    	
    
    	c.add(dest_path);
    	c.add(dest_folder);
    	c.add(date);
    	c.add(date_picker);
    	c.add(template_file);	
    	c.add(source_folder);
    	c.add(openButton);
    	File pathfile=new File("path_mc_template.txt");
        System.out.println(pathfile.exists());
		if(pathfile.exists()){
			System.out.println("Deven");
			try {
				BufferedReader in = new BufferedReader(new FileReader("path_mc_template.txt"));
				
				try {
					
					String destination=in.readLine();
					String date=in.readLine();
					String template_file=in.readLine();
					dest_folder.append(destination);
					date_picker.append(date);
					source_folder.append(template_file);
				
				in.close();
				
					
				} catch (IOException e) {
					
					e.printStackTrace();
				}
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		

 
  }
   

   public static final int trimRight( byte[] bytes, int pos )
   {
       if ( bytes == null )
       {
           return pos;
       }

       while ( ( pos >= 0 ) && ( bytes[pos] == ' ' ) )
       {
           pos--;
       }

       return pos;
   }


@Override
public void actionPerformed(ActionEvent e) {
	if(e.getActionCommand()=="Open")
	{
		
	 
		getfilepath();
		
	}
	
	
	// TODO Auto-generated method stub
	
    
	
     }


private void getfilepath()  {
	
    final JLabel statusbar = 
            new JLabel("Your Selection");  

	 JFileChooser chooser = new JFileChooser();
	    chooser.setMultiSelectionEnabled(true);
	    int option = chooser.showOpenDialog(SimpleFileChooser.this);
	    if (option == JFileChooser.APPROVE_OPTION) {
	    	
	    
	    		
	      File[] sf = chooser.getSelectedFiles();
	      
	      String filelist = "nothing";
	      if (sf.length > 0) filelist = sf[0].getName();
	     
	      
	      
	      
	      for (int l = 0; l < sf.length; l++) {

	        filelist += ", " + sf[l].getName();
	      
	        String filepath=sf[l].getAbsolutePath(); 
	        	
	        
	        
	      	//System.out.println(dest_folder.getText().toString());
	        	create_file(filepath,sf[l],dest_folder.getText());
				
			
	        
	       
	        
	      }


	      statusbar.setText("You chose " + filelist);
	      
	    }
	    else {
	      statusbar.setText("You canceled.");
	    }

	
}


private void create_file(String filepath,File sf,String destfolder)   {
    final Table table = new Table(new File(filepath));
    
    try
    {
        table.open(IfNonExistent.ERROR);

        final List<Field> fields = table.getFields();
        final Iterator<Record> recordIterator = table.recordIterator();
		String ext=sf.getName().substring(0,sf.getName().indexOf('.'));
		ext=ext.toLowerCase();
		
		
		String date=date_picker.getText();
		String destination=destfolder.concat(ext).concat("_").concat(date).concat(".xls");
		 
		String source=source_folder.getText();
				
		File pathfile=new File("path_mc_template.txt");
		
		pathfile.createNewFile();
		FileWriter fw = new FileWriter(pathfile.getAbsoluteFile());
		BufferedWriter out=new BufferedWriter(fw);
			
			out.write(dest_folder.getText()+"\r\n");
			out.write(date_picker.getText()+"\r\n");
			out.write(source_folder.getText()+"\r\n");
			out.close();
			FileInputStream file = new FileInputStream(new File(source));
			 
		    HSSFWorkbook workbook = new HSSFWorkbook(file);
		    HSSFSheet sheet = workbook.getSheetAt(0);
			
		    for(@SuppressWarnings("unused") final Field field: fields)
		       {
				
					 
					 
		       }


		
			try {
		    
			   
int j=1,i=0;				
		        while(recordIterator.hasNext())
		        {
		            final Record record = recordIterator.next();
		           
		            int count=6	;
		        	HSSFRow row = sheet.createRow(j);
		        	i=0;
		            for(final Field field: fields )
		            {
		                try
		                {
		                	if(count==0)
		                		break;
   							HSSFCell cell = row.createCell(i);
		   	              	String data_value=new String(record.getRawValue(field));
		   	              	
		   	              	if(i==3){
		   	              		data_value=data_value.substring(0,data_value.length()-3);
		   	              	cell.setCellValue(data_value);
		   	              	}
		   	              	
		   	              	
		   	              	else if(i==2){
		   	              		if(data_value.charAt(data_value.length()-1)=='0')
		   	              		{
		   	              			data_value=data_value.substring(0,data_value.length()-2);
		   	              			cell.setCellValue(data_value);
		   	              			
		   	              		}
		   	              	
		   	              	}
		   	              	else if(i==4){
		   	              	if(data_value.charAt(0)!=' ') 
		   	               		cell.setCellValue(data_value);
		   	              		//else System.out.println("Deven");
		   	              	}
		   	              	
		   	              	
		   	              	else if(i==5 ){
		   	              		
		   	              		if(data_value.charAt(0)!=' ') 
		   	               		cell.setCellValue(data_value);
		   	              		else System.out.println("Deven");
		   	              		
		   	              		
		   	              	}else 
		   	              	cell.setCellValue(data_value);
		   	              	
		   	                  	i++;  
		                    count--;
		                }
		                catch(ValueTooLargeException vtle)
		                {
		                }
		            }
		            j++;
		           
		        }

		
			    
		        FileOutputStream output = new FileOutputStream(destination);
				workbook.write(output);
				output.close();    
			    
			    
			    
			    
			    
			    
			    
			} catch (FileNotFoundException e) {
			    e.printStackTrace();
			} catch (IOException e) {
			    e.printStackTrace();
			}	
			
			
        
    }
    catch(IOException ioe)
    {
        System.out.println("Trouble reading table or table not found");
        ioe.printStackTrace();
    }
    catch(DbfLibException dbflibException)
    {
        System.out.println("Problem getting raw value");
        dbflibException.printStackTrace();
    }
    finally
    {
        try
        {
            table.close();
        } catch (IOException ex)
        {
            System.out.println("Unable to close the table");
        }
    }










	
}

 }
	

   
 
 